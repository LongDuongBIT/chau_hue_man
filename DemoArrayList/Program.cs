﻿using System;

namespace DemoArrayList
{
    public class CustomArrayList<T>
    {
        private T[] arr;

        private int count;

        public int Count
        {
            get { return count; }
        }

        public CustomArrayList(int INITIAL_CAPACITY = 5)
        {
            arr = new T[INITIAL_CAPACITY];
            count = 0;
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index > count)
                {
                    throw new ArgumentException("Chi so khong hop le");
                }
                return arr[index];
            }
            set
            {
                if (index < 0 || index > count)
                {
                    throw new ArgumentException("Chi so khong hop le");
                }
                arr[index] = value;
            }
        }

        private void GrowIfArrFull()
        {
            if (count >= arr.Length)
            {
                T[] newArray = new T[arr.Length * 2];
                Array.Copy(arr, newArray, count);
                arr = newArray;
            }
        }

        public void Add(T data)
        {
            GrowIfArrFull();
            arr[count] = data;
            count++;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index > count)
            {
                throw new ArgumentException("Chi so khong hop le");
            }
            Array.Copy(arr, index + 1, arr, index, count - index - 1);
            arr[count] = default(T);
            count--;
        }

        public int IndexOf(T data)
        {
            for (int i = 0; i < count; i++)
            {
                if (object.Equals(arr[i], data))
                {
                    return i;
                }
            }
            return -1;
        }

        public void Remove(T data)
        {
            int index = IndexOf(data);
            if (index != -1)
            {
                RemoveAt(index);
            }
            else
            {
                Console.WriteLine("Khong tim thay");
            }
        }

        public void Insert(int index, T data)
        {
            Array.Copy(arr, index, arr, index + 1, count - index - 1);
            arr[index] = data;
            count++;
        }

        public void Sort()
        {
            for (int i = 0; i < count - 1; i++)
            {
                for (int j = i + 1; j < count; j++)
                {
                    if (Convert.ToInt32(arr[i]) > Convert.ToInt32(arr[j]))
                    {
                        T temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }

        public void Reverse()
        {
            for (int i = 0; i < count / 2; i++)
            {
                T temp = arr[i];
                arr[i] = arr[count - i - 1];
                arr[count - i - 1] = temp;
            }
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            int[] arr = new int[5];
            CustomArrayList<int> list = new CustomArrayList<int>();
            list.Add(5);
            list.Add(5);
            list.Add(5);
            list.Add(8);
            list.Add(9);
            list.Add(4);
            list.Add(3);
            // list.RemoveAt(3);
            // list.Remove(9);
            // list.Insert(1, 9);
            list.Sort();
            list.Reverse();
            Console.WriteLine();
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
        }
    }
}