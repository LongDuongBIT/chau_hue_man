﻿using System;

namespace DemoLinkedList.cs
{
    internal class Node<T>
    {
        private T data;

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        private Node<T> link;

        public Node<T> Link
        {
            get { return link; }
            set { link = value; }
        }

        public Node()
        {
            this.data = default(T);
            this.link = null;
        }

        public Node(T Data)
        {
            this.data = Data;
            this.link = null;
        }

        public override string ToString()
        {
            return string.Format("{0}", data);
        }
    }

    internal class LinkedList<T>
    {
        private Node<T> head;
        private Node<T> tail;
        private int count;

        public Node<T> Head { get => head; set => head = value; }
        public Node<T> Tail { get => tail; set => tail = value; }

        public LinkedList()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public void AddFirst(T data)
        {
            Node<T> newNode = new Node<T>
            {
                Data = data
            };
            if (head == null)
            {
                head = tail = newNode;
                count++;
            }
            else
            {
                Node<T> current = head;
                head = newNode;
                newNode.Link = current;
                count++;
            }
        }

        public void AddLast(T data)
        {
            Node<T> newNode = new Node<T>
            {
                Data = data
            };
            if (head == null)
            {
                head = tail = newNode;
                count++;
            }
            else
            {
                Node<T> current = tail;
                current.Link = newNode;
                tail = newNode;
                newNode.Link = null;
            }
        }

        public void AddAt(int index, T data)
        {
            if (index < 0 || index > count)
            {
                throw new ArgumentException("Index khong hop le");
            }
            Node<T> newNode = new Node<T>(data);
            Node<T> current = head;
            for (int i = 0; i < index - 1; i++)
            {
                current = current.Link;
            }
            newNode.Link = current.Link;
            current.Link = newNode;
            count++;
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
        }
    }
}